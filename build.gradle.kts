import java.io.ByteArrayOutputStream
plugins {
    // Apply the java-library plugin to add support for Java Library
    java
}

repositories {
    mavenCentral()
}

sourceSets {
    main {
        java {
            srcDir("src")
        }
    }
}

dependencies {
    // Use JUnit test framework
    testImplementation("junit:junit:4.13.2")
    implementation(files("JPF/jpf-core/build/jpf-classes.jar"))
    implementation("io.vertx:vertx-core:4.2.6")
    implementation("com.github.javaparser:javaparser-symbol-solver-core:3.24.2")
    implementation("io.reactivex.rxjava3:rxjava:3.1.4")
}