package pcd.ass03.project1;

import pcd.ass03.project1.utility.Body;

import java.util.ArrayList;

public class Task {
    private String work = "";
    private ArrayList<Body> bodies;

    public Task(ArrayList<Body> bodies) {
        this.bodies = bodies;
    }

    public ArrayList<Body> getBodies() {
        return bodies;
    }
    public void setWork(String work) {
        this.work = work;
    }
    public String getWork() {
        return work.equals("") ? null : work;
    }
}
