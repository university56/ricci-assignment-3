package pcd.ass03.project2.Zone;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import pcd.ass03.project2.Utility.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ZoneActor extends AbstractActor {
    private HashMap<ActorRef, Triplet<Coordinate, Boolean, Boolean>> pluviometri = new LinkedHashMap<>(); //contiene un riferimento a tutti i pluviometri connessi
    //Triplet modella:              Coordinate, Attivo:true/false, Allarme:true/false
    private Area area;
    private ExecutorService executor = Executors.newCachedThreadPool();
    private boolean active_Challenge_Response = false; //false: not available for response; true: available
    private ArrayList<ActorRef> challenge_response = new ArrayList<>();



    @Override
    public synchronized Receive createReceive() {
        return receiveBuilder().match(Message.class, msg -> {
            ArrayList<Object> msg_Data = msg.getMessages();
            switch (msg.getMessageTypes()) {
                case SETUP -> area = (Area) msg_Data.get(0);
                case PLUVIOMETRO_CONNECT_TO_PARTITION -> {
                    Coordinate coordinate_Pluviometro = (Coordinate) msg_Data.get(0);
                    if(area.isCoordinateIsInsideArea(coordinate_Pluviometro)) {
                        pluviometri.put(getSender(), new Triplet<>(coordinate_Pluviometro, true, false));
                        getSender().tell(new Message(MessageTypes.PLUVIOMETRO_CONFIRM_CONNECTION), getSelf());
                    }
                }
            }
        }).build();
    }


    private void check_If_Pluviometri_Still_Active(int time_ChallengeResponse_s) {
        while (true) {
            active_Challenge_Response = true;
            for (ActorRef actorRef: pluviometri.keySet()) {
                actorRef.tell(new Message(MessageTypes.CHALLENGE_RESPONSE), ActorRef.noSender());
                challenge_response.add(actorRef);
            }

            //dopo che è stata inviata la challenge ai pluviometri, attendo X secondi.
            //è necessario che X sia abbastanza grande per ricevere-elaborare-inviare la risposta
            try {
                TimeUnit.SECONDS.sleep(time_ChallengeResponse_s);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            active_Challenge_Response = false; //disabilito la possibilità di ricevere response, il tempo dato è terminato
            boolean noChanges = true;
            //tutti quelli che non hanno risposto alla challenge_response, verranno considerati inattivi
            for (ActorRef actorRef: challenge_response) {
                System.out.println(actorRef + " risulta inattivo");
                pluviometri.get(actorRef).setB(false);
                noChanges = false;
            }
            challenge_response.clear();
            if(!noChanges) //se ci sono stati sensori che sono andati offline, allora devo ricalcolare l'allarme
                calculate_Allarm_Water();
            //Il check dei pluviometri viene ripetuto ogni 10 secondi
            try {
                TimeUnit.SECONDS.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("");
        }
    }
    private void calculate_Allarm_Water() {
        int active_Pluviometri = 0;
        int num_LivelloCritico = 0;
        for (ActorRef actorRef: pluviometri.keySet()) {
            Triplet informations = pluviometri.get(actorRef);
            if((Boolean) informations.getB())
                active_Pluviometri++;
            if((Boolean) informations.getC())
                num_LivelloCritico++;
        }
        int newactive_Pluviometri = (active_Pluviometri%2==0)?active_Pluviometri:active_Pluviometri+1;
        if(active_Pluviometri == 0)
            System.out.println("ZONA" + area + " non ha pluviometri attivi!; total: " + pluviometri.size());

        else if(num_LivelloCritico >= newactive_Pluviometri/2) {
            System.out.println("ZONA" + area + " è IN CODICE ROSSO CON VALORI " + active_Pluviometri + " " + num_LivelloCritico + "; total: " + pluviometri.size());
            //Simulation.view.tell(new Message(MessageTypes.PLUVIOMETRO_WATER_LEVEL_ALLARM_START, area.getArea_Name()), getSelf());
        }
        else
            System.out.println("ZONA " + area + " normale. " + active_Pluviometri + " " + num_LivelloCritico + "; total: " + pluviometri.size());
    }
}