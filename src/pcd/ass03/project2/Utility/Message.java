package pcd.ass03.project2.Utility;

import java.util.ArrayList;
import java.util.Arrays;

public final class Message {
    private final MessageTypes messageTypes;
    private ArrayList<Object> messages = new ArrayList<>();

    public Message(MessageTypes messageTypes) {
        this.messageTypes = messageTypes;
    }

    public Message(MessageTypes messageTypes, Object... messages) {
        this.messageTypes = messageTypes;
        this.messages.addAll(Arrays.asList(messages));
    }

    public MessageTypes getMessageTypes() { return messageTypes; }

    public ArrayList<Object> getMessages() {
        return messages;
    }
}
