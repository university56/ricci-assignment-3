package pcd.ass03.project1.actors;
import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import pcd.ass03.project1.Message;
import pcd.ass03.project1.MessageTypes;
import pcd.ass03.project1.utility.Body;
import pcd.ass03.project1.utility.Boundary;
import pcd.ass03.project1.utility.V2d;

import java.util.ArrayList;

/**
 *
 * This example is based on the previous Akka API 
 * 
 * @author aricci
 *
 */
public class CalculatorActor extends AbstractActor {
	ArrayList<Body> bodies = new ArrayList<>(); //contiene la lista di tutti i body
	ActorRef barrier;
	private double dt;
	private Boundary bounds;
	private int starting_Index;
	private int ending_Index;

	@Override
	public Receive createReceive() {
		return receiveBuilder().match(Message.class, msg -> {
			switch (msg.getMessageTypes()) {
				case SETUP -> {
					barrier = (ActorRef) msg.getMessage1();
					dt = (double) msg.getMessage2();
					bounds = (Boundary) msg.getMessage3();
				}
				case SET_INDEXES -> {
					starting_Index = (int) msg.getMessage1();
					ending_Index = (int) msg.getMessage2();
				}
				case CALCULATE_FORCES -> {
					bodies = (ArrayList<Body>) msg.getMessage1();
					ArrayList<Body> myListOfBodies = new ArrayList<>(bodies.subList(starting_Index, Math.min(ending_Index, bodies.size())));

					for (Body b: myListOfBodies) {
						b.updateVelocity(new V2d(computeTotalForceOnBody(b)).scalarMul(1.0 / b.getMass()), dt);
					}
					barrier.tell(new Message(MessageTypes.ACTOR_ENDED_COMPUTATION, myListOfBodies), ActorRef.noSender());
				}
				case UPDATE_NEW_POSITIONS -> {
					bodies = (ArrayList<Body>) msg.getMessage1();
					ArrayList<Body> myListOfBodies = new ArrayList<>(bodies.subList(starting_Index, Math.min(ending_Index, bodies.size())));

					for (Body b : myListOfBodies) {
						b.updatePos(dt);
						b.checkAndSolveBoundaryCollision(bounds);
					}
					barrier.tell(new Message(MessageTypes.ACTOR_ENDED_COMPUTATION, myListOfBodies), ActorRef.noSender());
				}
			}
		}).build();
	}
	private V2d computeTotalForceOnBody(Body b) {

		V2d totalForce = new V2d(0, 0);

		/* compute total repulsive force */
		for (int j = 0; j < bodies.size(); j++) {
			Body otherBody = bodies.get(j);
			if (!b.equals(otherBody)) {
				try {
					V2d forceByOtherBody = b.computeRepulsiveForceBy(otherBody);
					totalForce.sum(forceByOtherBody);
				} catch (Exception ex) {
				}
			}
		}

		/* add friction force */
		totalForce.sum(b.getCurrentFrictionForce());

		return totalForce;
	}
}
