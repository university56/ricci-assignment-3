package pcd.ass03.project2_OLD;

import akka.actor.ActorRef;
import com.github.javaparser.utils.Pair;
import pcd.ass03.project2.Utility.*;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

public class View {
    private JFrame f = new JFrame();
    private HashMap<JButton, ActorRef> zone_Buttons = new LinkedHashMap();

    public void startGUI(Pair<ArrayList<ActorRef>, ArrayList<Area>> partitions) {

        // get the content area of Panel.
        Container c = f.getContentPane();
        // set the LayoutManager
        c.setLayout(new BorderLayout());

        //Genero tutti i bottoni che corrispondono alle zone
        for(int i = 0; i < partitions.a.size(); i++ ) {
            Area area = partitions.b.get(i);
            JButton b1=new JButton(area.getArea_Name());
            b1.setBounds(area.getTop_Left_corner().getX(),area.getTop_Left_corner().getY(),area.get_Width(),area.get_Height());

            b1.addActionListener(e -> {
                if(zone_Buttons.get(b1) != null) {
                    zone_Buttons.get(b1).tell(new Message(MessageTypes.CASERMA_RICHIESTA_INTERVENTO), ActorRef.noSender());
                }
            });
            zone_Buttons.put(b1, null);
            f.add(b1);
        }
        JButton b1=new JButton("");
        b1.setBounds(-5,-5,0,0);
        f.add(b1);
        // set the size of the JFrame
        f.setSize(Data.CITY_WIDTH, Data.CITY_HEIGHT);
        // make the JFrame visible
        f.setVisible(true);
        // sets close behavior; EXIT_ON_CLOSE invokes System.exit(0) on closing the JFrame
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public void set_Zone_In_Allarm(String zone_Name, ActorRef actorRef) {
        for (JButton button: zone_Buttons.keySet()) {
            if(button.getText().equals(zone_Name)) {
                zone_Buttons.put(button, actorRef);
                button.setBackground(Color.RED);
            }
        }
    }
    public void remove_Zone_Allarm(String zone_Name) {
        for (JButton button: zone_Buttons.keySet()) {
            if(button.getText().equals(zone_Name)) {
                button.setBackground(Color.WHITE);
            }
        }
    }
}
