package pcd.ass03.project1;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import pcd.ass03.project1.actors.BarrierActor;
import pcd.ass03.project1.actors.CalculatorActor;
import pcd.ass03.project1.actors.DisplayActor;
import pcd.ass03.project1.actors.ExecutorActor;
import pcd.ass03.project1.utility.*;


import java.util.*;

public class Simulator {
	private static double dt = 0.001;
	private double vt = 0;
	private SimulationView viewer;
	/* bodies in the field */
	private ArrayList<Body> bodies;
	/* boundary of the field */
	private Boundary bounds;
	/* virtual time */
	private int GUI_width;
	private int GUI_height;

	private int nBodies;
	private int actorsNumber;
	private boolean enableGUI;

	public Simulator(int width, int height, boolean enableGUI, int actorsNumber, int bodiesNumber) {
		this.GUI_width = width;
		this.GUI_height = height;
		this.enableGUI = enableGUI;
		this.actorsNumber = actorsNumber;
		this.nBodies = bodiesNumber;
		if(actorsNumber > bodiesNumber) //se il numero di attori è maggiore del numero dei body, il numero di bodi viene impostato al nBodies
			this.actorsNumber = nBodies;
		bodiesInitialization();
	}
	private void bodiesInitialization() {
		bounds = new Boundary(-4.0, -4.0, 4.0, 4.0);
		Random rand = new Random();
		bodies = new ArrayList<Body>();
		for (int i = 0; i < nBodies; i++) {
			double x = bounds.getX0()*0.25 + rand.nextDouble() * (bounds.getX1() - bounds.getX0()) * 0.25;
			double y = bounds.getY0()*0.25 + rand.nextDouble() * (bounds.getY1() - bounds.getY0()) * 0.25;
			Body b = new Body(i, new P2d(x, y), new V2d(0, 0), 10);
			bodies.add(b);
		}
	}

	public void startSimulation(long nSteps) {
		ActorSystem system = ActorSystem.create("actorSystem");
		ArrayList<ActorRef> actors = new ArrayList<>();
		int[] partitions = splitIntoParts(nBodies, actorsNumber);


		//Avvio il programma principale di esecuzione
		ActorRef actorExecutor = system.actorOf(Props.create(ExecutorActor.class));

		//avvio la barrier di sincronizzazione
		ActorRef actorBarrier = system.actorOf(Props.create(BarrierActor.class));

		ActorRef actorView = null;
		//Start GUI or timer
		if(enableGUI) {
			this.viewer = new SimulationView(GUI_width,GUI_height);
			actorView = system.actorOf(Props.create(DisplayActor.class));
			actorView.tell(new Message(MessageTypes.SETUP, viewer, bounds, actorBarrier), ActorRef.noSender()); //invio la barrier
			actorView.tell(new Message(MessageTypes.UPDATE_VIEW, bodies, vt), ActorRef.noSender());
		}


		actorBarrier.tell(new Message(MessageTypes.SETUP, actorsNumber, actorExecutor, actorView), ActorRef.noSender());


		//Avvio gli attori adibiti al calcolo e imposto loro la barrier
		int startingPoint = 0;
		for(int i = 0; i < actorsNumber; i++) {
			ActorRef actorCalculator = system.actorOf(Props.create(CalculatorActor.class));
			actors.add(actorCalculator);
			actorCalculator.tell(new Message(MessageTypes.SETUP, actorBarrier, dt, bounds), ActorRef.noSender()); //invio la barrier
			actorCalculator.tell(new Message(MessageTypes.SET_INDEXES, startingPoint, startingPoint + partitions[i]), ActorRef.noSender());
			startingPoint += partitions[i];
		}

		//avvio il programma
		actorExecutor.tell(new Message(MessageTypes.SETUP, actors, nSteps), ActorRef.noSender());

		actorExecutor.tell(new Message(MessageTypes.START_PROGRAM, bodies), ActorRef.noSender());
	}

	private int[] splitIntoParts(int totalSize, int numberOfParts) {
		int[] arr = new int[numberOfParts];
		for (int i = 0; i < arr.length; i++)
			totalSize -= arr[i] = (totalSize + numberOfParts - i - 1) / (numberOfParts - i);
		return arr;
	}
}
