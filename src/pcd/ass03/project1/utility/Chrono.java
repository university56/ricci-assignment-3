package pcd.ass03.project1.utility;

import java.util.ArrayList;

public class Chrono {
    private boolean running;
    private double timer = 0.0;
    private ArrayList<Double> timerList = new ArrayList<>();

    public Chrono() {
        this.running = false;
    }

    public void start() {
        if(!running) {
            this.running = true;
            this.timer = System.currentTimeMillis();
        }
    }

    public void end() {
        if(running && timer > 0.0) {
            this.timer = System.currentTimeMillis() - timer;
            timerList.add(timer);
            this.running = false;
        }
    }

    public double getTimeInSeconds() {
        if(running)
            return (System.currentTimeMillis() - sumTime()) / 1000;
        else
            return sumTime() / 1000;
    }

    private double sumTime() {
        double totalTime = 0;
        if(timerList.size() > 0)
        {
            for (double partialtime: timerList) {
                totalTime += partialtime;
            }
        }
        return totalTime;
    }
}
