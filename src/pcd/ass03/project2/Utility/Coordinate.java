package pcd.ass03.project2.Utility;

public class Coordinate {
    private int x;
    private int y;

    public Coordinate(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }
    public int getY() {
        return y;
    }
    public String getCoordinatesAsString() {
        return x + " " + y;
    }
}
