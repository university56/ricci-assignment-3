package pcd.ass03.project2_OLD.Actors;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import pcd.ass03.project2.Utility.*;
import pcd.ass03.project2.Exceptions.PluviometroErrorConnectingException;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Pluviometro extends AbstractActor {
    private boolean water_Allarm_Active = false; //false: no allarm. true: water lever reached, allarm!
    private Coordinate posizione;
    private boolean system_Active = true;
    private ActorRef actor_Zone_Connected;
    private ExecutorService executor = Executors.newCachedThreadPool();

    @Override
    public Receive createReceive() {
        return receiveBuilder().match(Message.class, msg -> {
            ArrayList<Object> msg_Data = msg.getMessages();
            switch (msg.getMessageTypes()) {
                case SETUP -> {
                    posizione = (Coordinate) msg_Data.get(0);
                    connect_To_Area((ArrayList<Area>) msg_Data.get(1), (ArrayList<ActorRef>) msg_Data.get(2));

                    //SIMULATION PURPOSE
                    //executor.execute(this::randomly_Turn_OnOff);
                    executor.execute(this::randomly_Activate_Allarm);
                }
                case CHALLENGE_RESPONSE -> {
                    if(system_Active)
                        actor_Zone_Connected.tell(new Message(MessageTypes.CHALLENGE_RESPONSE), getSelf());
                }
            }
        }).build();
    }



    public void connect_To_Area(ArrayList<Area> areas, ArrayList<ActorRef> listOf_Partition_Actors) throws PluviometroErrorConnectingException {
        for(int i = 0; i < areas.size(); i++) {
            if(areas.get(i).isCoordinateIsInsideArea(posizione))
            {
                actor_Zone_Connected = listOf_Partition_Actors.get(i);
                listOf_Partition_Actors.get(i).tell(new Message(MessageTypes.PLUVIOMETRO_CONNECT_TO_PARTITION, posizione), getSelf());
                return; //nel il pluviometro capita nel bordo tra 2 zone, si aggancia alla 1° trovata
            }
        }
        throw new PluviometroErrorConnectingException();
    }


    public void randomly_Turn_OnOff() {
        int min = 4;
        int max = 7;
        Random r = new Random();
        while (true) {
            try {
                TimeUnit.SECONDS.sleep(r.nextInt(max-min) + min);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            system_Active = !system_Active;
        }
    }
    private void randomly_Activate_Allarm() {
        int min = 3;
        int max = 30;
        Random r = new Random();
        while (true) {
            try {
                TimeUnit.SECONDS.sleep(r.nextInt(max-min) + min);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if(!water_Allarm_Active)
                actor_Zone_Connected.tell(new Message(MessageTypes.PLUVIOMETRO_WATER_LEVEL_ALLARM_START), getSelf());
            water_Allarm_Active = true;
        }
    }
}
