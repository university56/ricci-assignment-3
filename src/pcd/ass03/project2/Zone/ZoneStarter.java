package pcd.ass03.project2.Zone;

import akka.actor.ActorSystem;
import com.typesafe.config.ConfigFactory;
import pcd.ass03.project2.Utility.*;
import pcd.ass03.project2.Exceptions.*;

import java.util.ArrayList;

public class ZoneStarter {
    public static void main(String[] args) throws PartitionOverlappingException, PartitionOutOfcityBounds, CityNotFullyCoveredException {
        ArrayList<pcd.ass03.project2.Utility.Area> existing_Areas = new ArrayList<>();

        ActorSystem system = ActorSystem.create("AkkaRemoteZone", ConfigFactory.load().getConfig("ZoneSystem"));
        System.out.println(system.provider().getDefaultAddress().port()); //debug. se legge 2552, ha ottenuto la configurazione



        //Create an actor
//        for(int i = 0; i < pcd.ass03.project2.Utility.Data.ZONE_COORDINATES.length; i++) {
//            int[] coordinates = getCoordinates(Data.ZONE_COORDINATES[i]);
//            pcd.ass03.project2.Utility.Area area = prenota_Area_Citta(coordinates, existing_Areas, ("Area " + i));
//            existing_Areas.add(area);
//            ActorRef actor_Zone_Created = system.actorOf(Props.create(ZoneActor.class), ("ZoneActor-"+i));
//            actor_Zone_Created.tell(new pcd.ass03.project2.Utility.Message(MessageTypes.SETUP, area), ActorRef.noSender());
//        }
//
//        check_City_Fully_Coverrage(existing_Areas);
//        System.out.println("Zones online");
    }



    static Area prenota_Area_Citta(int[] coordinates, ArrayList<Area> partitions, String area_Name) throws PartitionOverlappingException, PartitionOutOfcityBounds {
        //Controllo se i valori delle X rispettano i valori di base
        if((coordinates[0] < 0 || coordinates[2] < 0) ||
            (coordinates[0] > Data.CITY_WIDTH || coordinates[2] > Data.CITY_WIDTH))
            throw new PartitionOutOfcityBounds();
        //Controllo se i valori delle Y rispettano i valori di base
        if((coordinates[1] < 0 || coordinates[3] < 0) ||
            (coordinates[1] > Data.CITY_HEIGHT || coordinates[3] > Data.CITY_HEIGHT))
            throw new PartitionOutOfcityBounds();

        Area partition = new Area(new Coordinate(coordinates[0],coordinates[1]), coordinates[2], coordinates[3], area_Name);
        for (Area already_Existing_Partition: partitions) {
            if(already_Existing_Partition.isCoordinateIsInsideArea(new Coordinate(coordinates[0],coordinates[1])) ||
                already_Existing_Partition.isCoordinateIsInsideArea(new Coordinate(coordinates[0] + coordinates[2], coordinates[1] + coordinates[3])))
                throw new PartitionOverlappingException();
        }
        return partition;
    }
    static int[] getCoordinates(String coordinates_Data) {
        String[] notConvertedCoords = coordinates_Data.replace(" ", "").split(",");
        return new int[] {
            Integer.parseInt(notConvertedCoords[0]),
            Integer.parseInt(notConvertedCoords[1]),
            Integer.parseInt(notConvertedCoords[2]),
            Integer.parseInt(notConvertedCoords[3])
        };
    }
    static void check_City_Fully_Coverrage(ArrayList<Area> areas) throws CityNotFullyCoveredException {
        int total_Area = 0;
        for (Area area: areas) {
            total_Area += area.getArea();
        }
        if(total_Area != Data.CITY_HEIGHT * Data.CITY_WIDTH)
            throw new CityNotFullyCoveredException();
    }
}
