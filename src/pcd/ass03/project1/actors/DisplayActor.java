package pcd.ass03.project1.actors;
import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import pcd.ass03.project1.Message;
import pcd.ass03.project1.MessageTypes;
import pcd.ass03.project1.SimulationView;
import pcd.ass03.project1.utility.Body;
import pcd.ass03.project1.utility.Boundary;

import java.util.ArrayList;

/**
 *
 * This example is based on the previous Akka API 
 * 
 * @author Lazzari Matteo
 *
 */
public class DisplayActor extends AbstractActor {
	private SimulationView viewer;
	private Boundary bounds;
	private ActorRef barrierActor;
	private long iter = 0;
	private double vt = 0;
	@Override
	public Receive createReceive() {
		return receiveBuilder().match(Message.class, msg -> {
			switch (msg.getMessageTypes()) {
				case SETUP -> {
					viewer = (SimulationView) msg.getMessage1();
					bounds = (Boundary) msg.getMessage2();
					barrierActor = (ActorRef) msg.getMessage3();
				}
				case UPDATE_VIEW -> {
					ArrayList<Body> bodies = (ArrayList<Body>) msg.getMessage1();
					double value = (double) msg.getMessage2();
					vt = ((value != Double.MIN_VALUE) ? value : vt);
					viewer.display(bodies, vt, iter, bounds); //load initial view
					iter++;
					barrierActor.tell(new Message(MessageTypes.ACTOR_ENDED_COMPUTATION, new ArrayList<Body>()), ActorRef.noSender());
				}
			}
		}).build();
	}
}
