package pcd.ass03.project2.Utility;

/***
 * La classe contiene tutte le informazioni riguardanti l'applicazione
 */
public class Data {

    public static final String[] ZONE_COORDINATES = new String[] {
        "0,0,300,300",
        "300,0,300,300",
        "0,300,300,300",
        "300,300,300,300",
        "0,600,600,200"
//        "0,0,300,800",
//        "300,0,300,800"
    };
    public static final int CITY_WIDTH = 600;
    public static final int CITY_HEIGHT = 800;
    public static final int NUMBER_OF_PLUVIOMETRI = 10;
}
