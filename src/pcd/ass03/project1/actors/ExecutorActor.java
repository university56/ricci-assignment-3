package pcd.ass03.project1.actors;
import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import org.checkerframework.checker.units.qual.C;
import pcd.ass03.project1.Message;
import pcd.ass03.project1.MessageTypes;
import pcd.ass03.project1.utility.Body;
import pcd.ass03.project1.utility.Chrono;

import java.util.ArrayList;

/**
 *
 * This example is based on the previous Akka API 
 * 
 * @author aricci
 *
 */
public class ExecutorActor extends AbstractActor {
	private Chrono chrono = new Chrono();
	private ArrayList<ActorRef> actors = new ArrayList<>();
	private long totalCycles;
	private long actualCyclesDone = 0;
	private boolean keepServeNewCalls = true;
	private boolean calculateForceCycle = true;

	@Override
	public Receive createReceive() {
		return receiveBuilder().match(Message.class, msg -> {
			switch (msg.getMessageTypes()) {
				case SETUP -> {
					actors = (ArrayList<ActorRef>) msg.getMessage1();
					totalCycles = (long) msg.getMessage2();
				}
				case START_PROGRAM -> {
					chrono.start();
					if(keepServeNewCalls) {
						if(calculateForceCycle)
							System.out.println();
						if(calculateForceCycle)
							for (ActorRef actor: actors) {
								actor.tell(new Message(MessageTypes.CALCULATE_FORCES, (ArrayList<Body>)msg.getMessage1()), ActorRef.noSender());
							}
						else
							for (ActorRef actor: actors) {
								actor.tell(new Message(MessageTypes.UPDATE_NEW_POSITIONS, (ArrayList<Body>)msg.getMessage1()), ActorRef.noSender());
							}
						actualCyclesDone++;
						calculateForceCycle = !calculateForceCycle;
						if(actualCyclesDone == 2*totalCycles) {
							chrono.end();
							keepServeNewCalls = false;
							System.out.println("Execution time: " + chrono.getTimeInSeconds() + " s");
							System.exit(0);
						}
					}
				}
			}
		}).build();
	}
}
