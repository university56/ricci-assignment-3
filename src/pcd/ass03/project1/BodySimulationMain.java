package pcd.ass03.project1;

/**
 * Bodies simulation
 * 
 * @author Lazzari Matteo matteo.lazzari7@studio.unibo.it
 */
public class BodySimulationMain {

    public static void main(String[] args) throws InterruptedException {
        Simulator sim = new Simulator(800, 800, true, 5, 1000);
        sim.startSimulation(500000);
    }
}